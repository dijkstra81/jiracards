﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using yossarian.JiraCards;


namespace yossarian.JiraCards.Tests
{
    [TestFixture]
    public class OptionsTest
    {
        [Test]
        public void GetQuery_ReturnsEmptyStringIfAdditionalQueryIsEmpty()
        {
            string[] args = new string[] {"" };
            var option = Options.Parse(args);
            Assert.AreEqual(String.Empty, option.GetQuery());
        }

        [Test]
        public void GetQuery_ReturnsOrderedQueryIfAdditionalQueryIsNotEmpty()
        {
            string validQuery = "valid query";
            string[] args = new string[] { validQuery };
            var option = Options.Parse(args);

            Assert.AreEqual(validQuery+" ORDER BY id", option.GetQuery());
        }
    }
}
