﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PdfSharp.Drawing;

namespace yossarian.JiraCards.Helper
{
    // point, but in cms
    sealed class XCmPoint
    {
        public XCmPoint(XCm cmX, XCm cmY)
        {
            X = cmX;
            Y = cmY;
        }
        public XCm X { get; private set; }
        public XCm Y { get; private set; }

        public static XCmPoint operator +(XCmPoint p1, XCmSize p2)
        {
            return new XCmPoint(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static implicit operator XPoint(XCmPoint p)
        {
            return new XPoint(p.X.GetPoints(), p.Y.GetPoints());
        }
    }
}
