﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PdfSharp.Drawing;

namespace yossarian.JiraCards.Helper
{
    /// <summary>
    /// Used for protecting units that are in CM from those that are in displaysize
    /// 
    /// there are no operators with 'double' intentionally!
    /// </summary>
    [DebuggerDisplay("{Value}cm")]
    sealed class XCm
    {
        public double Value { get; private set; }
        public static readonly XCm Zero = new XCm(0);

        public XCm(double value)
        {
            Value = value;
        }


        public static XCm FromPoints(double h)
        {
            return new XCm(XUnit.FromPoint(h).Centimeter);
        }

        /// <summary>
        /// return value in actual screen coords
        /// </summary>
        /// <returns></returns>
        public double GetPoints()
        {
            return XUnit.FromCentimeter(Value).Point;
        }

        /*public static implicit operator double(XCm c)
        {
            return c.GetPoints();
        }*/

        public static XCm operator +(XCm c1, XCm c2)
        {
            return new XCm(c1.Value + c2.Value);
        }

        public static XCm operator -(XCm c1, XCm c2)
        {
            return new XCm(c1.Value - c2.Value);
        }

        public static XCm operator /(XCm c1, int c2)
        {
            return new XCm(c1.Value / c2);
        }

        public static XCm operator /(int c2, XCm c1)
        {
            return new XCm(c1.Value / c2);
        }

        public static double operator /(XCm c1, XCm c2)
        {
            return c1.Value / c2.Value;
        }

        public static XCm operator *(XCm c1, int c2)
        {
            return new XCm(c1.Value * c2);
        }

        public static XCm operator *(int c2, XCm c1)
        {
            return new XCm(c1.Value * c2);
        }

    }
}
