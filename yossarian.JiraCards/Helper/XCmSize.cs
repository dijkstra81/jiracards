using System.Diagnostics;
using PdfSharp.Drawing;

namespace yossarian.JiraCards.Helper
{
    [DebuggerDisplay("{X.Value}cm x {Y.Value}cm")]
    sealed class XCmSize
    {
        public XCmSize(XCm cmX, XCm cmY)
        {
            X = cmX;
            Y = cmY;
        }
        public XCm X { get; private set; }
        public XCm Y { get; private set; }

        public static implicit operator XVector(XCmSize s)
        {
            return new XVector(s.X.GetPoints(), s.Y.GetPoints());
        }

        public static explicit operator XCmPoint(XCmSize s)
        {
            return new XCmPoint(s.X, s.Y);
        }

        public static XCmSize operator *(XCmSize s1, int i)
        {
            return new XCmSize(s1.X * i, s1.Y * i);
        }

        public static XCmSize operator /(XCmSize s1, int i)
        {
            return new XCmSize(s1.X / i, s1.Y / i);
        }

        public static XCmSize operator -(XCmSize s1, XCmSize s2)
        {
            return new XCmSize(s1.X - s2.X, s1.Y - s2.Y);
        }
    }
}