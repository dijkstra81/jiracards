using System;
using System.Runtime.Serialization;

namespace yossarian.JiraCards
{
    [Serializable]
    public class OptionsParsingException : ApplicationException
    {
        public OptionsParsingException()
        {
        }

        public OptionsParsingException(string message) : base(message)
        {
        }

        protected OptionsParsingException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}