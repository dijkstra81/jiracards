﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Principal;
using System.Threading;
using CommandLine;
using CommandLine.Text;
using yossarian.JiraCards.Properties;

namespace yossarian.JiraCards
{
    internal class Options
    {
        private Options()
        { }

        [Option('u', "user", Required = false, HelpText = "User used for connecting to Jira. If not specified, local one is used", DefaultValue = "")]
        public string User { get; set; }

        [Option('p', "password", Required = false, HelpText = "Password used for connecting to Jira. If not specified, prompt on CLI is done", DefaultValue = "")]
        public string Password { get; set; }

        [Option('s', "server", Required = false, HelpText = "Jira server", DefaultValue = "")]
        public string Jira { get; set; }      

        [Option('e', "export", DefaultValue="", HelpText = "Exports the jira js to given file")]
        public string Export { get; set; }

        [Option('i', "import", DefaultValue = "", HelpText = "Imports tasks from given js file")]
        public string Import { get; set; }

        [Option('d', "debug", DefaultValue = false, HelpText = "Wait for debugger attached")]
        public bool WaitForDebugger { get; set; }

        [ValueOption(0)]
        public string AdditionalQuery { get; set; }


        [HelpOption]
        public string GetUsage()
        {
            var help = new HelpText
            {
                Heading = new HeadingInfo("JiraCards"),
                Copyright = new CopyrightInfo("vit.tauer@solarwinds.com", 2015),
                AdditionalNewLineAfterOption = true,
                AddDashesToOption = true
            };

            help.AddOptions(this);

            if (this.LastParserState.Errors.Any())
            {
                var errors = help.RenderParsingErrorsText(this, 2); // indent with two spaces

                if (!string.IsNullOrEmpty(errors))
                {
                    help.AddPreOptionsLine(string.Concat(Environment.NewLine, "ERROR(S):"));
                    help.AddPreOptionsLine(errors);
                }
            }

            return help;
        }

        [ParserState]
        public IParserState LastParserState { get; set; }

        public static Options Parse(string[] args)
        {
            var opt = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args, opt))
            {
                if (opt.WaitForDebugger)
                {
                    Console.WriteLine("Waiting for debugger to be attached ...");
                    while (!Debugger.IsAttached)
                    {
                        Thread.Sleep(1);
                    }
                }
                return opt;
            }
            else
            {
                throw new OptionsParsingException();
            }
        }

        /// <summary>
        /// returns server, either from cmdline, or from settings
        /// </summary>
        /// <returns></returns>
        public string GetServer()
        {
            if (string.IsNullOrWhiteSpace(Jira))
            {
                return Settings.Default.DefaultServer;
            }
            else
            {
                return Jira;
            }
        }

        /// <summary>
        /// returns either user from cmdline, or name of current user
        /// </summary>
        /// <returns></returns>
        public string GetUser()
        {
            if (string.IsNullOrWhiteSpace(User))
            {
                return RemoveDomain(WindowsIdentity.GetCurrent().Name);
            }
            else
            {
                return User;
            }
        }

        /// <summary>
        /// Returns password from cmdline, if not there, asks
        /// </summary>
        /// <returns></returns>
        public string GetPassword(string user)
        {
            if (string.IsNullOrWhiteSpace(Password))
            {
                Console.Write("Password for user({0}): ", user);

                Password = ConvertToUnsecureString(ScanPassword());
            }

            return Password;
        }

        private static string ConvertToUnsecureString(SecureString securePassword)
        {
            if (securePassword == null)
                throw new ArgumentNullException("securePassword");

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

        public SecureString ScanPassword()
        {
            var pwd = new SecureString();
            while (true)
            {
                ConsoleKeyInfo i = Console.ReadKey(true);
                if (i.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    break;
                }
                else if (i.Key == ConsoleKey.Backspace)
                {
                    if (pwd.Length > 0)
                    {
                        pwd.RemoveAt(pwd.Length - 1);
                        Console.Write("\b \b");
                    }
                }
                else
                {
                    pwd.AppendChar(i.KeyChar);
                    Console.Write("*");
                }
            }
            pwd.MakeReadOnly();
            return pwd;
        }

        private static string RemoveDomain(string p)
        {
            var un = p.Split('\\');
            if (un.Length == 1)
                return un[0];
            else
                return un[1];
        }

        /// <summary>
        /// returns constructed query
        /// </summary>
        /// <returns></returns>
        public string GetQuery()
        {
            string query = String.Empty;

            if (!string.IsNullOrWhiteSpace(AdditionalQuery))
            {
                query = AdditionalQuery + " ORDER BY id";
            }

            return query;
        }
    }
}