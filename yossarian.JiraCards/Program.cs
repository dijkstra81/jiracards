﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Atlassian.Jira;
using Atlassian.Jira.Remote;
using Newtonsoft.Json;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using yossarian.JiraCards.Helper;
using yossarian.JiraCards.Rendering;

namespace yossarian.JiraCards
{
    class Program
    {
        // rectangle size
        private static readonly XCm IssueHeight = new XCm(6.0); // cm
        private static readonly XCm IssueWidth = new XCm(13.0); // cm

        private static readonly XCm MinimalMargin = new XCm(1.0); // cm
        private static readonly XCm TextPadding = new XCm(0.2); // cm

        private static readonly XCm FontSize = new XCm(0.8); // cm, size of em
        private static readonly XCm FontSizeDescription = new XCm(0.7); // cm, size of em
        private const string FontFaceName = "Arial";

        private Options _options;

        public Program(Options options)
        {
            _options = options;
        }

        static int Main(string[] args)
        {
            try
            {
                var program = new Program(Options.Parse(args));

                program.RunMain();
            }
            catch (OptionsParsingException oex)
            {
                if (!string.IsNullOrWhiteSpace(oex.Message))
                    Console.Error.WriteLine(oex.Message);

                return 1;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                return 1;
            }

            return 0;
        }

        private void RunMain()
        {
            IEnumerable<Issue> issues;
            if (!string.IsNullOrWhiteSpace(_options.Import))
            {
                using (var f = new StreamReader(File.Open(_options.Import, FileMode.Open, FileAccess.Read, FileShare.Read)))
                {
                    issues = ImportIssues(f);
                }
            }
            else
            {               
                    issues = GetIssues();
            }
            if (!string.IsNullOrWhiteSpace(_options.Export))
            {
                using (var f = new StreamWriter(File.Open(_options.Export, FileMode.Create, FileAccess.Write, FileShare.None)))
                {
                    ExportIssues(issues, f);
                }
            }
            else
            {
                var doc = CreateCards(issues);
                var path = Path.GetTempFileName() + ".pdf";
                doc.Save(path);
                Process.Start(path);
            }
        }

        private IEnumerable<Issue> ImportIssues(TextReader file)
        {
            var serializer = new JsonSerializer();

            var remoteIssues = serializer.Deserialize<RemoteIssue[]>(new JsonTextReader(file));
            return remoteIssues.Select(issue => issue.ToLocal());
        }

        private void ExportIssues(IEnumerable<Issue> issues, TextWriter textWriter)
        {
            var issuesArray = issues.Select(x => x.ToRemote()).ToArray();
            var serializer = new JsonSerializer();

            serializer.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            serializer.Formatting = Formatting.Indented;

            serializer.Serialize(textWriter, issuesArray);
        }

        private IEnumerable<Issue> GetIssues()
        {
            var user = _options.GetUser();
            var jira = new Jira(_options.Jira, user, _options.GetPassword(user));
            var q = _options.GetQuery();

            return jira.GetIssuesFromJql(q);            
        }

        private static PdfDocument CreateCards(IEnumerable<Issue> issues)
        {
            

            var pageProperties = new RenderProperties
            (
                issueSize : new XCmSize(IssueWidth, IssueHeight),
                minimalMargin : MinimalMargin,
                fontSizeTitle : FontSize,
                fontSizeDescription : FontSizeDescription,
                fontFaceName : FontFaceName,
                textPadding : TextPadding
            );

            var pageRenderer = new PageRenderer(pageProperties);

            return pageRenderer.CreateDocument(issues);
        }

        
    }
}
