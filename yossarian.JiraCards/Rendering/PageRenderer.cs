﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlassian.Jira;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using yossarian.JiraCards.Helper;

namespace yossarian.JiraCards.Rendering
{
    class PageRenderer
    {
        private const string StoryPointsCustomPropertyId = "customfield_10008";

        private readonly RenderProperties _renderProperties;
        public PageRenderer(RenderProperties renderProperties)
        {
            _renderProperties = renderProperties;
        }

        public PdfDocument CreateDocument(IEnumerable<Issue> issues)
        {
            var doc = new PdfDocument();

            var renderingContext = new RenderingContext(doc, _renderProperties);

            foreach (var issue in issues)
            {
                var issuePosition = renderingContext.GetNextIssuePosition();

                RenderIssue(renderingContext, issue, issuePosition);
            }

            while (renderingContext.IssuesOnThisPage < renderingContext.RenderProperties.PageProperties.IssuesPerPage - 1)
            {
                RenderIssue(renderingContext, null, renderingContext.GetNextIssuePosition());
            }

            return doc;
        }

        private static void RenderIssue(RenderingContext context, Issue issue, XCmPoint issuePosition)
        {
            var rectangle = new XRect(
                issuePosition,
                context.RenderProperties.PageProperties.IssueSize
            );            

            XBrush brush = GetBrushForIssueType(context, issue);
            var storyPointsRectangleSize = new XCmSize(context.RenderProperties.StringHeightWithMargin, context.RenderProperties.StringHeightWithMargin);

            var storyPointsRectangle = new XRect(rectangle.TopRight -
                new XCmSize(context.RenderProperties.StringHeightWithMargin, XCm.Zero), storyPointsRectangleSize);

            var titleRectangle = new XRect(rectangle.TopLeft,
                new XCmSize(context.RenderProperties.IssueSize.X, context.RenderProperties.StringHeightWithMargin)
                );

            RenderRectangle(context, titleRectangle, brush);
            var descriptionRectangle = new XRect(
                rectangle.TopLeft + new XCmSize(XCm.Zero, context.RenderProperties.StringHeightWithMargin) + context.RenderProperties.TextPaddingSize,
                rectangle.BottomRight - context.RenderProperties.TextPaddingSize);


            RenderRectangles(context, 
                rectangle, storyPointsRectangle, titleRectangle);

            if (issue != null)
            {
                RenderIssueText(context, issue, descriptionRectangle, storyPointsRectangle, titleRectangle);
            }
        }

        private static XBrush GetBrushForIssueType(RenderingContext context, Issue issue)
        {
            XBrush brush = new XSolidBrush(XColor.FromKnownColor(XKnownColor.White));
            if (issue != null)
            {
                if (issue.Type.IsSubTask)
                {
                    brush = context.RenderProperties.SubtaskBrush;
                }
                else
                {
                    switch (issue.Type.Name)
                    {
                        case "Story":
                            brush = context.RenderProperties.StoryBrush;
                            break;
                        case "Research":
                            brush = context.RenderProperties.ResearchBrush;
                            break;
                        case "Bug":
                            brush = context.RenderProperties.BugBrush;
                            break;
                    }
                }
            }
            return brush;
        }

        private static CustomFieldValue FindCustomField(Issue i, string id)
        {
            return i.CustomFields.FirstOrDefault(cf => cf.Id == id);
        }

        private static void RenderIssueText(RenderingContext context, Issue issue, XRect descriptionRectangle, XRect storyPointsRectangle, XRect titleRectangle)
        {
            string storyPoints;

            var fiss = FindCustomField(issue, StoryPointsCustomPropertyId);
            if (fiss != null)
            {
                storyPoints = fiss.Values[0];
            }
            else
            {
                if (issue.Type.IsSubTask)
                    storyPoints = "ST";
                else
                    storyPoints = "";
            }

            //issue.ParentIssueKey is still empty https://bitbucket.org/farmas/atlassian.net-sdk/issues/164/how-to-get-parentissuekey-of-a-sub-task           
            var tf = new XTextFormatter(context.Graphics);
            tf.DrawString(issue.Summary, context.RenderProperties.FontRegular, context.RenderProperties.BlackBrush, descriptionRectangle, XStringFormats.TopLeft);
            tf.DrawString(issue.Key.Value, context.RenderProperties.FontBold, context.RenderProperties.BlackBrush,
                new XRect(
                    titleRectangle.TopLeft + context.RenderProperties.TextPaddingSize,
                    titleRectangle.BottomRight),
                XStringFormats.TopLeft);

            if (string.IsNullOrWhiteSpace(storyPoints))
            {
                context.Graphics.DrawLine(context.RenderProperties.BlackPen, storyPointsRectangle.TopLeft, storyPointsRectangle.BottomRight);
                context.Graphics.DrawLine(context.RenderProperties.BlackPen, storyPointsRectangle.TopRight, storyPointsRectangle.BottomLeft);
            }
            else
            {
                context.Graphics.DrawString(storyPoints, context.RenderProperties.FontBold, context.RenderProperties.BlackBrush, storyPointsRectangle, new XStringFormat
                {
                    Alignment = XStringAlignment.Center,
                    LineAlignment = XLineAlignment.Center
                });
            }        
        }

        private static void RenderRectangles(RenderingContext context, params XRect[] rects)
        {
            foreach (var rect in rects)
            {
                context.Graphics.DrawRectangle(context.RenderProperties.BlackPen, rect);
            }
        }

        private static void RenderRectangle(RenderingContext context, XRect rect, XBrush brush)
        {
            context.Graphics.DrawRectangle(brush, rect);
        }

    }
}
