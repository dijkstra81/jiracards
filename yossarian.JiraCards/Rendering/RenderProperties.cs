﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using yossarian.JiraCards.Helper;

namespace yossarian.JiraCards.Rendering
{
    sealed class RenderProperties
    {
        


        public RenderProperties(XCmSize issueSize, XCm minimalMargin, XCm fontSizeTitle, XCm fontSizeDescription, string fontFaceName, XCm textPadding)
        {
            BlackPen = new XPen(XColor.FromKnownColor(XKnownColor.Black), 1);
            BlackBrush = new XSolidBrush(XColor.FromKnownColor(XKnownColor.Black));
            BugBrush = new XSolidBrush(XColor.FromKnownColor(XKnownColor.OrangeRed));
            ResearchBrush = new XSolidBrush(XColor.FromKnownColor(XKnownColor.SeaGreen));
            StoryBrush = new XSolidBrush(XColor.FromKnownColor(XKnownColor.Gold));
            SubtaskBrush = new XSolidBrush(XColor.FromKnownColor(XKnownColor.DeepSkyBlue));


            A4Width = new XCm(29.7);
            A4Height = new XCm(21.0);

            IssueSize = issueSize;
            MinimalMargin = minimalMargin;
            FontSizeTitle = fontSizeTitle;
            FontSizeDescription = fontSizeDescription;
            FontFaceName = fontFaceName;
            TextPadding = textPadding;

            FontRegular = new XFont(FontFaceName, FontSizeDescription.GetPoints(), XFontStyle.Regular, new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Default));
            FontBold = new XFont(FontFaceName, FontSizeTitle.GetPoints(), XFontStyle.Bold, new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Default));
            TextPaddingSize = new XCmSize(TextPadding, TextPadding);

            StringHeight = XCm.FromPoints(FontBold.GetHeight());
            StringHeightWithMargin = StringHeight + TextPadding;

            PageProperties = new PageProperties(new XCmSize(A4Width, A4Height), IssueSize, MinimalMargin);


        }

        public XFont FontBold { get; private set; }
        public XFont FontRegular { get; private set; }

        public XBrush BlackBrush { get; private set; }
        public XBrush BugBrush { get; private set; }
        public XBrush ResearchBrush { get; private set; }
        public XBrush StoryBrush { get; private set; }
        public XBrush SubtaskBrush { get; private set; }

        public XPen BlackPen { get; private set; }

        public XCmSize IssueSize { get; private set; }
        public XCm MinimalMargin { get; private set; }
        public XCm FontSizeTitle { get; private set; }
        public XCm FontSizeDescription { get; private set; }
        public string FontFaceName { get; private set; }
        public XCm TextPadding { get; private set; }

        public XCm A4Width { get; private set; }
        public XCm A4Height { get; private set; }
        public XCmSize TextPaddingSize { get; private set; }

        public XCm StringHeight { get; private set; }
        public XCm StringHeightWithMargin { get; private set; }

        public PageProperties PageProperties { get; private set; }
    }
}