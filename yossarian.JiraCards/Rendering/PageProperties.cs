using System;
using PdfSharp.Drawing;
using yossarian.JiraCards.Helper;

namespace yossarian.JiraCards.Rendering
{
    sealed class PageProperties
    {
        public PageProperties(XCmSize pageSize, XCmSize issueSize, XCm minimalMargin)
        {
            IssueSize = issueSize;
            IssuesOnRow = Math.Min((int)Math.Floor(pageSize.X / (issueSize.X + minimalMargin)), (int)Math.Floor(pageSize.X / issueSize.X));
            IssuesRows = Math.Min((int)Math.Floor(pageSize.Y / (issueSize.Y + minimalMargin)), (int)Math.Floor(pageSize.Y / issueSize.Y));
            IssuesPerPage = IssuesOnRow * IssuesRows;

            Margin = new XCmSize( 
                (pageSize.X - IssuesOnRow*issueSize.X)/IssuesOnRow,
                (pageSize.Y - IssuesRows*issueSize.Y)/IssuesRows);


        }

        public XCmSize IssueSize { get; private set; }

        public int IssuesOnRow { get; private set; }
        public int IssuesRows { get; private set; }
        public int IssuesPerPage { get; private set; }


        public XCmSize Margin { get; private set; }

        public XCmPoint InitialIssuePosition
        {
            get { return (XCmPoint)(Margin/2); }
        }


        /// <summary>
        /// Returns position of issue on following line to previous one
        /// </summary>
        /// <param name="issuePosition"></param>
        /// <returns></returns>
        public XCmPoint IssueCrLf(XCmPoint issuePosition)
        {
            return new XCmPoint(Margin.X / 2, issuePosition.Y + IssueSize.Y + Margin.Y);
        }

        /// <summary>
        /// Returns position of issue on following position in row
        /// </summary>
        /// <param name="issuePosition"></param>
        /// <returns></returns>
        public XCmPoint IssueTab(XCmPoint issuePosition)
        {
            return issuePosition + new XCmSize(IssueSize.X + Margin.X, XCm.Zero);
        }
    }
}