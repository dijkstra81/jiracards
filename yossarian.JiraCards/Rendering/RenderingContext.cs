﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using yossarian.JiraCards.Helper;

namespace yossarian.JiraCards.Rendering
{
    sealed class RenderingContext
    {
        private readonly PdfDocument _doc;
        private readonly RenderProperties _renderProperties;

        public RenderProperties RenderProperties { get { return _renderProperties; } }

        private XCmPoint _issuePosition;
        public RenderingContext(PdfDocument doc, RenderProperties renderProperties)
        {
            _doc = doc;
            _renderProperties = renderProperties;
        }

        public PdfPage Page { get; private set; }
        public XGraphics Graphics { get; private set; }
        public int IssuesOnThisPage { get; private set; }
        public int IssuesOnThisRow { get; private set; }

        
        private void CreatePage()
        {
            var page = new PdfPage { Orientation = PageOrientation.Landscape, Size = PageSize.A4 };

            _doc.Pages.Add(page);
            Graphics = XGraphics.FromPdfPage(page);

            Page = page;
        }


        public XCmPoint GetNextIssuePosition()
        {
            if (Page == null || ++IssuesOnThisPage == _renderProperties.PageProperties.IssuesPerPage)
            {
                CreatePage();
                IssuesOnThisPage = 0;
                IssuesOnThisRow = 0;

                _issuePosition = _renderProperties.PageProperties.InitialIssuePosition;
            }
            else
            {
                if (++IssuesOnThisRow == _renderProperties.PageProperties.IssuesOnRow)
                {
                    _issuePosition = _renderProperties.PageProperties.IssueCrLf(_issuePosition);
                    IssuesOnThisRow = 0;
                }
                else
                {
                    _issuePosition = _renderProperties.PageProperties.IssueTab(_issuePosition);
                }
            }

            return _issuePosition;
        }
    }
}
